#Android Client: Mountain Rescue
This project is the mobile application counterpart of Mountain Rescue project.
Currently we provide only an android app
##Overview
The app will use Wifi-P2P to search for and connect to Rescue Stations(raspberry PIs). Users will be able to use the app to
1) Download detail map of local region including directions to spots like nearest water source, next station, high ground etc.

2) Send distress signal to authorities in case there is no mobile telecommunication network.

3) In normal scenario just register at the station to leave a trail. This will help authorities to track down hikers incase some go missing.
